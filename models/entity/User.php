<?php

namespace application\models\entity;


class User
{
    private $id;
    private $name;
    private $files = [];


    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function appendFile(File $file)
    {
        $this->files[] = $file;
    }

    public function calcCountAvgRowsInFile()
    {
        $rows_count = 0;

        /**
         * @var File $file
         */
        foreach ($this->files as $file) {
            $rows_count += $file->getRowsCount();
        }

        return $rows_count / count($this->files);
    }

    public function replaceDateAndCopyFiles()
    {
        $replace_count = 0;

        /**
         * @var File $file
         */
        foreach ($this->files as $file) {
            $replace_count += $file->replaceDateAndCopy();
        }

        return $replace_count;
    }
}