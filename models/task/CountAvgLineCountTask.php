<?php

namespace application\models\task;


use application\models\entity\User;

class CountAvgLineCountTask extends Task
{

    public function exec()
    {
        /**
         * @var User $user
         */

        print("Count average rows in file:" . PHP_EOL);

        foreach ($this->users as $user) {
            $rows_count_avg = $user->calcCountAvgRowsInFile();
            print($user->getName() . ": " . $rows_count_avg . PHP_EOL);
        }
    }
}