<?php

namespace application\models\entity;


class File
{
    const INPUT_TEXT_DIR = './texts';
    const OUTPUT_TEXT_DIR = './output_texts';

    private $filename;

    public function __construct($filename)
    {
        $this->filename = $filename;
    }

    public function getRowsCount()
    {
        return count(file(self::INPUT_TEXT_DIR . "/" . $this->filename));
    }

    public function replaceDateAndCopy()
    {
        $file_content = file_get_contents(self::INPUT_TEXT_DIR . "/" . $this->filename);

        $changed_text = preg_replace_callback(
            '/(\d{2})\/(\d{2})\/(\d{2})/',
            function ($matches) {
                $matches[3] = ($matches[3] < 70) ? "20{$matches['3']}" : "19{$matches['3']}";
                return $matches[2] . '-' . $matches[1] . '-' . $matches[3];
            },
            $file_content,
            -1,
            $replace_count
        );

        file_put_contents(self::OUTPUT_TEXT_DIR . "/" . $this->filename, $changed_text);

        return $replace_count;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }


}