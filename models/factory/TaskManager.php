<?php

namespace application\models\factory;


use application\models\task\CountAvgLineCountTask;
use application\models\task\Task;
use application\models\task\ReplaceDatesTask;
use InvalidArgumentException;

class TaskManager
{
    /**
     * @param string $task_type
     * @param string $delimiter
     * @return Task
     */
    public static function factory($task_type, $delimiter)
    {
        switch ($task_type) {
            case 'countAverageLineCount':
                $task = new CountAvgLineCountTask($delimiter);
                break;

            case 'replaceDates':
                $task = new ReplaceDatesTask($delimiter);
                break;

            default:
                throw new InvalidArgumentException("Invalid task");
                break;
        }

        return $task;
    }
}