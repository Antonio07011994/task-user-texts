<?php

namespace application\models\task;


use application\models\entity\File;
use application\models\entity\User;
use InvalidArgumentException;

abstract class Task
{
    const INPUT_TEXT_DIR = './texts';
    const OUTPUT_TEXT_DIR = './output_texts';

    protected $users;

    public function __construct($delimiter)
    {
        $this->loadUserList($delimiter);
        $this->loadUserFiles();
    }

    private function loadUserList($delimiter)
    {
        switch ($delimiter) {
            case 'comma':
                $d = ',';
                break;

            case 'semicolon':
                $d = ';';
                break;

            default:
                throw new InvalidArgumentException('Invalid delimiter type');
                break;
        }

        $handle = fopen('./people.csv', 'r');

        while (($user = fgetcsv($handle, 0, $d)) !== false) {

            if (count($user) !== 2) {
                continue;
            }

            $this->users[$user[0]] = new User($user[0], str_replace(PHP_EOL, '', $user[1]));
        }
    }

    private function loadUserFiles()
    {
        $files = scandir(self::INPUT_TEXT_DIR);

        foreach ($files as $filename) {
            $user_id = explode('-', $filename)[0];

            if (!isset($this->users[$user_id])) {
                continue;
            }

            $this->users[$user_id]->appendFile(new File($filename));
        }
    }

    abstract public function exec();
}