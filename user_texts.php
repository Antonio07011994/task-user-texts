<?php

use application\models\UserTextsApp;

require_once './models/factory/TaskManager.php';
require_once './models/task/Task.php';
require_once './models/task/CountAvgLineCountTask.php';
require_once './models/task/ReplaceDatesTask.php';
require_once './models/UserTextsApp.php';
require_once './models/entity/User.php';
require_once './models/entity/File.php';

$app = new UserTextsApp([$argv[1], $argv[2]]);
$app->run();