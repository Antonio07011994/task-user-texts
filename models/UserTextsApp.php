<?php

namespace application\models;

use application\models\factory\TaskManager;
use Exception;
use InvalidArgumentException;

class UserTextsApp
{
    const ALLOWED_SEPARATORS = [
        'comma',
        'semicolon'
    ];

    const ALLOWED_TASKS = [
        'countAverageLineCount',
        'replaceDates'
    ];

    private $input;
    private $task;
    private $separator;

    public function __construct($parameters)
    {
        $this->input = $parameters;
    }

    public function run()
    {
        try {

            $this->validate();
            $this->parseInput();
            TaskManager::factory($this->task, $this->separator)->exec();

        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    private function parseInput()
    {
        foreach ($this->input as $param) {
            if (in_array($param, self::ALLOWED_SEPARATORS)) {
                if (isset($this->separator)) {
                    throw new InvalidArgumentException('You can not specify more than 2 separators');
                }

                $this->separator = $param;
                continue;
            }

            if (in_array($param, self::ALLOWED_TASKS)) {
                if (isset($this->task)) {
                    throw new InvalidArgumentException('You can not specify more than 2 tasks');
                }

                $this->task = $param;
                continue;
            }

            throw new InvalidArgumentException('Invalid parameter');
        }
    }

    private function validate()
    {
        switch (count($this->input) <=> 2) {
            case -1: throw new InvalidArgumentException('Too few parameters');
            case 1 : throw new InvalidArgumentException('Too many parameters');
        }
    }
}