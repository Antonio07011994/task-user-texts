<?php

namespace application\models\task;


use application\models\entity\User;

class ReplaceDatesTask extends Task
{

    public function exec()
    {
        print("Replace Count: " . PHP_EOL);

        /**
         * @var User $user
         */
        foreach ($this->users as $user) {
            $replace_count = $user->replaceDateAndCopyFiles();
            print($user->getName() . ": " . $replace_count . PHP_EOL);
        }
    }
}